// Action creators

export const selectSong = (song) =>{ // Named export - import { selectSong } from ../actions/index.js
    // Return an action
    return {
        type: 'SONG_SELECTED',
        payload: song
    };
};

