import React from 'react';
import SongList from './SongList.js';
import SongDetail from './SongDetail.js';

const App = () =>{


    return (
        <div className="container mt-5">
            <div className="row">
                <div className="col-lg-6">    
                    <SongList />
                </div>
                <div className="col-lg-6">
                    <SongDetail />       
                </div>
            </div>
        </div>
    );
};

export default App;