import React from 'react';
import { connect } from 'react-redux';

const SongDetail = (props) =>{
    console.log(props);
    return (
        <div>
            {props.song ? 
            <div>
                <h3>Title: {props.song.title}</h3>
                <h3>Duration: {props.song.duration}</h3>
            </div> 
            : 
            <div>
                 <h3>Select a song</h3>
            </div>} 
        </div>
    )
};

const mapStateToProps = (state) =>{
    return { song: state.selectedSong }
};

export default connect(mapStateToProps)(SongDetail);