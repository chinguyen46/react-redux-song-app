import React from 'react';
import { connect } from 'react-redux';
import { selectSong } from '../actions/index.js';

class SongList extends React.Component {

    renderList(){
        return this.props.songs.map(song => { 
            return (
                <div className="list-group-item" key={song.title}>
                    <div className="float-right">
                        <button onClick={() => this.props.selectSong(song)} className="btn btn-primary btn-md">Select</button>
                    </div>
                    <div className="">{song.title}</div>
                </div>            
            );
        });
    };


    render(){
        return <div className="list-group">{this.renderList()}</div>
    }

};

const mapStateToProps = (state) =>{
    return { songs: state.songs }; // Return an object that will show up as prop inside of component
};

export default connect(mapStateToProps, {
    selectSong: selectSong
})(SongList); 
