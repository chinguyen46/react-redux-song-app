import React from 'react';
import ReactDOM from 'react-dom';
//Named exports
import { Provider } from 'react-redux'; //Importing Provider component from react-redux
import { createStore } from 'redux'; //Importing createStore function from redux

//Default exports
import App from './components/App.js';
import reducers from './reducers/index.js';

ReactDOM.render(
<Provider store={createStore(reducers)}>
    <App />
</Provider>, 
document.querySelector('#root'));