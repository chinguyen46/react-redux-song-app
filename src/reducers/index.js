import { combineReducers } from 'redux';


// Reducers
const songsReducer = () =>{
    return [
            {title: 'No Scrubs', duration: '4:05'},
            {title: 'Last Christmas', duration: '3:30'},
            {title: 'All Star', duration: '3:45'},
            {title: 'Unbreak My Heart', duration: '3:10'}
            ];
};

const selectedSongReducer = (selectedSong = null, action) =>{
    if(action.type === 'SONG_SELECTED'){
        return action.payload; 
    }else{
        return selectedSong;
    }
};

export default combineReducers({
    songs: songsReducer,
    selectedSong: selectedSongReducer
});